﻿<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <!-- The configuration and platform will be used to determine which assemblies to include from solution and
				 project documentation sources -->
    <Configuration Condition=" '$(Configuration)' == '' ">Debug</Configuration>
    <Platform Condition=" '$(Platform)' == '' ">AnyCPU</Platform>
    <SchemaVersion>2.0</SchemaVersion>
    <ProjectGuid>{f576bb65-c337-453a-b957-e40c40ccc41d}</ProjectGuid>
    <SHFBSchemaVersion>2015.6.5.0</SHFBSchemaVersion>
    <!-- AssemblyName, Name, and RootNamespace are not used by SHFB but Visual Studio adds them anyway -->
    <AssemblyName>Documentation</AssemblyName>
    <RootNamespace>Documentation</RootNamespace>
    <Name>Documentation</Name>
    <!-- SHFB properties -->
    <FrameworkVersion>.NET Framework 4.5.2</FrameworkVersion>
    <OutputPath>.\Help\</OutputPath>
    <HtmlHelpName>Documentation</HtmlHelpName>
    <Language>en-US</Language>
    <HelpFileVersion>1.0.0.0</HelpFileVersion>
    <MaximumGroupParts>2</MaximumGroupParts>
    <NamespaceGrouping>False</NamespaceGrouping>
    <SyntaxFilters>C#</SyntaxFilters>
    <SdkLinkTarget>Blank</SdkLinkTarget>
    <RootNamespaceContainer>True</RootNamespaceContainer>
    <PresentationStyle>VS2013</PresentationStyle>
    <Preliminary>False</Preliminary>
    <NamingMethod>HashedMemberName</NamingMethod>
    <HelpTitle>StarkTools - StreamUtilities</HelpTitle>
    <ContentPlacement>BelowNamespaces</ContentPlacement>
    <DocumentationSources>
      <DocumentationSource sourceFile="..\..\Generic C# Project\StarkTools\StarkTools.csproj" />
      <DocumentationSource sourceFile="..\..\Generic C# Project\StarkTools\bin\Debug\StarkTools-StreamUtilities.dll" />
      <DocumentationSource sourceFile="..\..\Generic C# Project\StarkTools\bin\Debug\StarkTools-StreamUtilities.xml" />
    </DocumentationSources>
    <SaveComponentCacheCapacity>100</SaveComponentCacheCapacity>
    <BuildAssemblerVerbosity>OnlyWarningsAndErrors</BuildAssemblerVerbosity>
    <HelpFileFormat>HtmlHelp1, Website</HelpFileFormat>
    <IndentHtml>False</IndentHtml>
    <KeepLogFile>True</KeepLogFile>
    <DisableCodeBlockComponent>False</DisableCodeBlockComponent>
    <CleanIntermediates>True</CleanIntermediates>
    <MissingTags>Summary, Parameter, AutoDocumentCtors, Namespace, TypeParameter, AutoDocumentDispose</MissingTags>
    <NamespaceSummaries>
      <NamespaceSummaryItem name="System.IO" isDocumented="True">The System.IO namespace contains types that allow reading and writing to files and data streams, and types that provide basic file and directory support.</NamespaceSummaryItem>
      <NamespaceSummaryItem name="(global)" isDocumented="False" />
    </NamespaceSummaries>
    <FeedbackEMailLinkText>Raphael.StarkTools.Assistance%40gmail.com</FeedbackEMailLinkText>
    <FeedbackEMailAddress>Raphael.StarkTools.Assistance%40gmail.com</FeedbackEMailAddress>
    <CopyrightText>Created by Raphael Tetreault %282016%29</CopyrightText>
    <ProjectSummary>StarkTools-StreamUtilities is a set of tools designed to read and write binary streams in either Big-Endian or Little-Endian with minial effort in a simple and flexibility manner. It began as a tool to reverse engineer binary game files that managed it&amp;#39%3bs data in lean, meta-less binary formats. As such, it caters towards reading and writting binary where the user has complete knowledge of the data&amp;#39%3bs serialization. This means that while these tools make it easier to manage binary files, it requires the user to know the exacts of the format they are dealing with and a solid understanding of binary. These tools are not an end-all-be-all solution, but instead make it easier to manage binary formats in a more flexible way.</ProjectSummary>
    <ComponentConfigurations>
      <ComponentConfig id="Code Block Component" enabled="False" xmlns="">
        <component id="Code Block Component">
          <!-- Base path for relative filenames in source attributes (optional) -->
          <basePath value="{@HtmlEncProjectFolder}" />
          <!-- Base output paths for the files (required).  These should match the parent folder of the output path
	 of the HTML files (see each of the SaveComponent instances in the configuration files). -->
          <outputPaths>
	{@HelpFormatOutputPaths}
</outputPaths>
          <!-- Allow missing source files (Optional).  If omitted, it will generate errors if referenced source files
	 are missing. -->
          <allowMissingSource value="false" />
          <!-- Remove region markers from imported code blocks.  If omitted, region markers in imported code blocks
	 are left alone. -->
          <removeRegionMarkers value="false" />
          <!-- Code colorizer options (required).
	 Attributes:
		Language syntax configuration file (required)
		XSLT style sheet file (required)
		CSS style sheet file (required)
		Script file (required)
		Disabled (optional, leading whitespace normalization only)
		Default language (optional)
		Enable line numbering (optional)
		Enable outlining (optional)
		Keep XML comment "see" tags within the code (optional)
		Tab size override (optional, 0 = Use syntax file setting)
		Use language name as default title (optional) -->
          <colorizer syntaxFile="{@SHFBFolder}PresentationStyles\Colorizer\highlight.xml" styleFile="{@SHFBFolder}PresentationStyles\Colorizer\highlight.xsl" stylesheet="{@SHFBFolder}PresentationStyles\Colorizer\highlight.css" scriptFile="{@SHFBFolder}PresentationStyles\Colorizer\highlight.js" disabled="{@DisableCodeBlockComponent}" language="cs" numberLines="false" outlining="false" keepSeeTags="false" tabSize="0" defaultTitle="true" />
        </component>
      </ComponentConfig>
      <ComponentConfig id="Reflection Index Data (SQL Cache)" enabled="False" xmlns="">
        <component id="Reflection Index Data (SQL Cache)">
          <index name="reflection" value="/reflection/apis/api" key="@id" cache="15" localCacheSize="2500" cacheProject="false" connectionString="">
            <data base="{@FrameworkReflectionDataFolder}" recurse="true" files="*.xml" duplicateWarning="false" groupId="ReflectionIndexCache">
		{@ReferenceLinkNamespaceFiles}
	</data>
            <data files="reflection.xml" groupId="Project_Ref_{@UniqueID}" />
          </index>
          <copy name="reflection" source="*" target="/document/reference" />
        </component>
      </ComponentConfig>
      <ComponentConfig id="IntelliSense Component" enabled="False" xmlns="">
        <component id="IntelliSense Component">
          <!-- Output options (optional)
  Attributes:
    Include namespaces (false by default)
    Namespaces comments filename ("Namespaces" if not specified or empty)
    Output folder (current folder if not specified or empty) -->
          <output includeNamespaces="false" namespacesFile="Namespaces" folder="{@OutputFolder}" />
        </component>
      </ComponentConfig>
    </ComponentConfigurations>
    <TransformComponentArguments>
      <Argument Key="logoFile" Value="logo" xmlns="" />
      <Argument Key="logoHeight" Value="" xmlns="" />
      <Argument Key="logoWidth" Value="" xmlns="" />
      <Argument Key="logoAltText" Value="" xmlns="" />
      <Argument Key="logoPlacement" Value="left" xmlns="" />
      <Argument Key="logoAlignment" Value="left" xmlns="" />
      <Argument Key="maxVersionParts" Value="" xmlns="" />
      <Argument Key="defaultLanguage" Value="cs" xmlns="" />
      <Argument Key="includeEnumValues" Value="true" xmlns="" />
    </TransformComponentArguments>
    <RootNamespaceTitle>StarkTools - StreamUtilities</RootNamespaceTitle>
  </PropertyGroup>
  <!-- There are no properties for these groups.  AnyCPU needs to appear in order for Visual Studio to perform
			 the build.  The others are optional common platform types that may appear. -->
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|Win32' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|Win32' ">
  </PropertyGroup>
  <!-- Import the SHFB build targets -->
  <Import Project="$(SHFBROOT)\SandcastleHelpFileBuilder.targets" />
  <!-- The pre-build and post-build event properties must appear *after* the targets file import in order to be
			 evaluated correctly. -->
  <PropertyGroup>
    <PreBuildEvent>
    </PreBuildEvent>
    <PostBuildEvent>
    </PostBuildEvent>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
  </PropertyGroup>
</Project>