﻿// Created by Raphael "Stark" Tetreault /2017
// Copyright (c) 2017 Raphael Tetreault
// Last updated 

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Demo : MonoBehaviour
{
	protected virtual void OnEnable()
	{
        // An example stream storing
        // Decimal, 4 ints, string, 2 floats, double, long, ulong
        // For readability, each newline is set after 16 bytes
        BufferedStream s = new BufferedStream(new MemoryStream());
        BinaryWriter writer = new BinaryWriter(s, Encoding.ASCII);

        //dynamic[] dyn = new dynamic[0x100000];
        //for (int i = 0; i < dyn.Length; ++i)
        //    dyn[i] = i;

        float timestamp = Time.time;
        BinaryStreamUtility.WriteDynamicDataRecursively(
            writer,
            (Application.persistentDataPath + "/tempfile.txt"),

            //dyn
            new decimal(128392743264982),
            new int[] { 50, 60, 70, 80 },
            "This is 16 chars",
            3.14f, 6.28f, 12.5663706144d,
            long.MinValue, ulong.MaxValue,
            Test.a, Test.b, Test.c, Test.d
            );

        Debug.Log(Time.time - timestamp);

        // Displays stream in console
        //s.DebugStream();

        // Serializes stream to current directory
        //StreamUtility.SerializeStream(s, Directory.GetCurrentDirectory(), "StreamUtilityTest", "bin");
    }

    public enum Test
    {
        a = 1 << 8,
        b = 1 << 9,
        c = 1 << 10,
        d = 1 << 10,
    }

}