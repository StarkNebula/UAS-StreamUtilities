﻿//using System;
//using System.IO;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace StarkTools
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {

//            // An example stream storing
//            // Decimal, 4 ints, string, 2 floats, double, long, ulong
//            // For readability, each newline is set after 16 bytes
//            BufferedStream s = new BufferedStream(new MemoryStream());
//            BinaryWriter writer = new BinaryWriter(s, Encoding.ASCII);

//            StreamUtility.RecursiveDynamicDataWriter(
//                writer,

//                new decimal(128392743264982),
//                new int[] { 50, 60, 70, 80 },
//                "This is 16 chars",
//                3.14f, 6.28f, 12.5663706144d,
//                long.MinValue, ulong.MaxValue
//                );

//            // Displays stream in console
//            //s.DebugStream();

//            // Serializes stream to current directory
//            StreamUtility.SerializeStream(s, Directory.GetCurrentDirectory(), "StreamUtilityTest", "bin");
//        }
//    }
//}
